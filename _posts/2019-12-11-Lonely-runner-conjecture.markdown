---
layout: post
title:  "Lonely runner conjecture"
date:   2019-12-11 20:40:03 +0100
categories: Weekly conjecture
---
In number theory, and especially the study of diophantine approximation, the lonely runner conjecture is a conjecture originally due to J. M. Wills in 1967. Applications of the conjecture are widespread in mathematics; they include view obstruction problems[1] and calculating the chromatic number of distance graphs and circulant graphs.[2] The conjecture was given its picturesque name by L. Goddyn in 1998.[3]

Consider k runners on a circular track of unit length. At t = 0, all runners are at the same position and start to run; the runners' speeds are pairwise distinct. A runner is said to be lonely at time t if they are at a distance of at least 1/k from every other runner at time t. The lonely runner conjecture states that each runner is lonely at some time.


[[1] T. W. Cusick (1973). "View-Obstruction problems".][1]

[1]: https://link.springer.com/article/10.1007%2FBF01832623
