---
layout: post
title:  "Eulers conjecture on sums of like powers"
date:   2019-12-04 20:00:00 +0100
categories: Weekly conjecture
---
Euler conjectured that at least $$n$$ n-th powers are required for $$n>2$$ to provide a sum that is itself an n-th power, i.w. we are looking for a set of numbers $$ a_1,\dots,a_n$$, such that:
<br/><br/>$$
\begin{equation}
	a_1^n...
\end{equation}
$$<br/><br/>

The conjecture is a generalisation to Fermats famous last theorem for the special case of $$n=2$$. Eulers conjecture was disproved by Lander and Parkin (1967) with the counterexample
<br/><br/>$$
\begin{equation}
	27^5 + 84^5 + 110^5 + 133^5 = 144^5
\end{equation}
$$<br/><br/>
and is thus no longer a conjecture. The fastest code we came up with to find a set of numbers similar to the original disproove is 
{% highlight c++%}
for(int a = 2; a<N; a++){
  for(int b = a * pow(1. / 4,1./5); b < a ; b++){
    tmp = pow(po[a] -po[b],1./5);
    cmax = tmp < b ? tmp : b ;
    for(int c = pow((po[a] - po[b]) / 3.,1./5); c <= cmax ; c++){
      tmp = pow(po[a] - po[b] - po[c],1./5);
      dmax = tmp < c ? tmp : c;
      for(int d = pow((po[a] - po[b] - po[c]) / 2.,1./5); d <= dmax; d++){
      	tmp = (po[a] - po[b] - po[c] - po[d]);
	if(pow(round(pow(tmp,1./5)),5) == tmp){
	  cout << "Found new set of numbers" << endl;
	  cout <<a<<"  "<<b<<"  "<<c<<"  "<<d<<"  "<<pow(tmp,1./5)<< endl;
        }
      }
    }
  }
}
{% endhighlight c++%}

We replaced the exponentiations with lookups in an array that is filled via
{% highlight c++%}
for(int i=0; i<N; i++){
  po[i] = pow(i,5);
}


{% endhighlight c++%}
[1]: https://link.springer.com/article/10.1007%2FBF01832623
